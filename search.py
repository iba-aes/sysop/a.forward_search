import os
import sys


if __name__ == "__main__":
   root_dir = sys.argv[1]#main(sys.argv[1:])
   resultFiles = []
   for root, dirs ,files in os.walk(root_dir,topdown=True):
       #print(root)
       user_forward = []
       for file in files:
           if(file.startswith(".forward")):
               print("Found .forward", os.path.join(root,file))
               user_forward.append(os.path.join(root,file))
               dirs[:] = []
       if(len(user_forward) > 0):
           resultFiles.append(user_forward)
   #output = open("out.txt","r")
   with open("out.txt","r") as output:
       #do something to save the file.
